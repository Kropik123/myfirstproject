//=============================================================================
// MySecondPlugin.js
//=============================================================================

/*:
 * @plugindesc second plugin ... yeah!
 * @author Riccardo Kropik
 *
 * @param Array Parameter
 * @desc bla
 * @default 1,3,4
 *
 * @help This plugin does not provide plugin commands.
 */

/*:ja
 * @plugindesc -
 * @author Riccardo Kropik
 *
 * @help -
 */
 
(function(){
	 
	//var _Game_System.prototype.isMenuEnabled = Game_System.prototype.isMenuEnabled;
	var parameters = PluginManager.parameters('MySecondPlugin');
	var maps = String(parameters['Array Parameter']).split(',');
	 
	Game_System.prototype.isMenuEnabled = function(){
		//return (_Game_System.prototype.isMenuEnabled.call(this) && (maps.contains(String($gameMap.mapId())) == false));
		return this._menuEnabled && maps.contains(String($gameMap.mapId())) == false;
	};
	 
})();
 
/*
 (function() {
	 
	//var _Game_System.prototype.isMenuEnabled = Game_System.prototype.isMenuEnabled;
	var parameters = PluginManager.parameters('MySecondPlugin');
	var maps = String(parameters['Array Parameter']).split(',');
	 
	if(!maps.contains(String($gameMap.mapId))
		Game_System.prototype.disableMenu.call(this);
	 
 })();
*/