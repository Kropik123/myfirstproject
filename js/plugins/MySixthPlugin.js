//=============================================================================
// MySixthPlugin.js
//============================================================================

/*:
 * @plugindesc iwas
 * @author Riccardo Kropik
 *
 *
 * @help This plugin does not provide plugin commands.
 */

/*:ja
 * @plugindesc -
 * @author Riccardo Kropik
 *
 * @help -
 */

(function() {
	
	var notetagsLoaded = false;
	var _DataManager_isDatabaseLoaded = DataManager.isDatabaseLoaded;
	
	DataManager.isDatabaseLoaded = function() {
		if(!_DataManager_isDatabaseLoaded.call(this))
			return false;
		else
		{
			if(notetagsLoaded === false)
			{
				var regexTpCost = /<TP Cost:\s*(\d+)>/i;
				for(var i = 0; i < $dataSkills.length; i++){
					if($dataSkills[i])
					{
						var match = $dataSkills[i].note.match(regexTpCost);
						if(match)
						{
							$dataSkills[i].MySixthPluginTpCost = Number(match[1]);
						}
					}
				}
				notetagsLoaded = true;
			}
			return true;
		}	
	};
	
	Game_BattlerBase.prototype.skillTpCost = function(skill) {
		
		if(skill.MySixthPluginTpCost)
		{
			return skill.MySixthPluginTpCost
		}
		else
			return skill.tpCost;
	};

})();