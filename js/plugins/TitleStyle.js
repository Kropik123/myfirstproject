//=============================================================================
// TitleStyle.js
//=============================================================================

/*:
 * @plugindesc changes the titlemenu appearrance
 * @author Riccardo Kropik
 *
 * @param color
 * @desc border-color of the font
 * @default green
 *
 * @param outline-width
 * @desc width of the outline
 * @default 8
 *
 * @param font-size
 * @desc font-size of the title
 * @default 78
 *
 * @help This plugin does not provide plugin commands.
 */

/*:ja
 * @plugindesc -
 * @author Riccardo Kropik
 *
 * @help -
 */

(function(){

	var parameters = PluginManager.parameters('TitleStyle');
	
	var color = String(parameters['color']);
	var outlineWidth = Number(parameters['outline-width']);
	var fontSize = Number(parameters['font-size']);
	

	Scene_Title.prototype.drawGameTitle = function() {
		var x = 20;
		var y = Graphics.height / 4;
		var maxWidth = Graphics.width - x * 2;
		var text = $dataSystem.gameTitle;
		this._gameTitleSprite.bitmap.outlineColor = color;
		this._gameTitleSprite.bitmap.outlineWidth = outlineWidth;
		this._gameTitleSprite.bitmap.fontSize = fontSize;
		this._gameTitleSprite.bitmap.drawText(text, x, y, maxWidth, 48, 'center');
	};
	
})();
