//=============================================================================
// MySecondPlugin.js
//=============================================================================

/*:
 * @plugindesc second plugin ... yeah!
 * @author Riccardo Kropik
 *
 * @param Boolean Parameter
 * @desc Input either true or false
 * @default true
 *
 * @help This plugin does not provide plugin commands.
 */

/*:ja
 * @plugindesc -
 * @author Riccardo Kropik
 *
 * @help -
 */
 
(function(){
	 
	//var _Game_System.prototype.isMenuEnabled = Game_System.prototype.isMenuEnabled;
	var parameters = PluginManager.parameters('MyThirdPlugin');
	
	var bool = String(parameters['Boolean Parameter']).trim() == "true";
	 
	Window_TitleCommand.prototype.makeCommandList = function() {
		this.addCommand(TextManager.newGame, 'newGame');
		this.addCommand(TextManager.continue_, 'continue', this.isContinueEnabled());
		if(bool)
		{
			this.addCommand(TextManager.options, 'options',);
		}
	};
		
})();