//=============================================================================
// MyFourthPlugin.js
//============================================================================

/*:
 * @plugindesc fourth plugin ... yeah!
 * @author Riccardo Kropik
 *
 *
 * @help This plugin does not provide plugin commands.
 */

/*:ja
 * @plugindesc -
 * @author Riccardo Kropik
 *
 * @help -
 */
 
(function(){
	
	var _Game_Interpreter_pluginCommand = Game_Interpreter.prototype.pluginCommand;
	
	Game_Interpreter.prototype.pluginCommand = function(command, args) {
		
		_Game_Interpreter_pluginCommand.call(this, command, args);
		
		var nOne = Number(args[0]);
		var nTwo = Number(args[1]);
		
		if(command == "addition")
			$gameMessage.add(String(nOne + nTwo));
		else if(command == "subtraction")
			$gameMessage.add(String(nOne - nTwo));
		else if(command == "division")
			$gameMessage.add(String(nOne / nTwo));
		else if(command == "multiplication")
			$gameMessage.add(String(nOne * nTwo));
	};
		
})();