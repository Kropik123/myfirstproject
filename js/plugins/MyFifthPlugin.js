//=============================================================================
// MyFifthPlugin.js
//============================================================================

/*:
 * @plugindesc adds the actor-title to the drawn name
 * @author Riccardo Kropik
 *
 *
 * @help This plugin does not provide plugin commands.
 */

/*:ja
 * @plugindesc -
 * @author Riccardo Kropik
 *
 * @help -
 */

(function() {
	Window_Base.prototype.drawActorName = function(actor, x, y, width) {
		width = width || 168;
		this.changeTextColor(this.hpColor(actor));
		
		var title;
		if($dataActors[actor.actorId()].meta.title)
			title = $dataActors[actor.actorId()].meta.title + " ";
		else
			title = "";
		
		this.drawText(title + actor.name(), x, y, width);
	};
})();