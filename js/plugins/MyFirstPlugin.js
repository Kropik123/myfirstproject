(function () {
	/*
	var _Game_Player_executeMove = Game_Player.prototype.executeMove;
	Game_Player.prototype.executeMove = function(direction){
		
		_Game_Player_executeMove.call(this, direction);
		
		$gameMessage.add("Hello World");
	};
	*/
	
	var _Game_Player_canEncounter = Game_Player.prototype.canEncounter;
	Game_Player.prototype.canEncounter = function(){
		return (_Game_Player_canEncounter.call(this) && !$gamePlayer.isDashing());
	};
})();
